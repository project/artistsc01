<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->status == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; print ' '. $zebra; ?>">
  <div class="clear-block">
    <div class="comment-header">
      <span class="title">
        <?php print $title ?>
        <?php if ($comment->new) : ?>
          <a id="new"></a><span class="new"><?php print drupal_ucfirst($new) ?></span>
        <?php endif; ?>
      </span>
    </div>
    <?php if ($submitted): ?>
      <span class="submitted"><?php print t('!date — !username', array('!username' => theme('username', $comment), '!date' => format_date($comment->timestamp))); ?></span>
    <?php endif; ?>
    <?php print $picture ?>
    <div class="content">
      <?php print $content ?>
    </div>
  </div>
  <?php if ($links): ?>
    <div class="comment-footer">
      <div class="links"><?php print $links ?></div>
    </div>
  <?php endif; ?>
</div>
