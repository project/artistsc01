<?php

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
//function _phptemplate_variables($hook, $vars) {
function phptemplate_preprocess_page(&$vars) {
    $arBodyClass = array();

    // if it's not front page and show current page in breadcrumbs are turned on, add current page to breadcrumbs
    if (!drupal_is_front_page()) {
      if (theme_get_setting('artistsC01_showCurrentInBreadcrumbs')) {
        $breadcrumbs = drupal_get_breadcrumb();
        // add current page
        $breadcrumbs[] = drupal_get_title();
        $vars['breadcrumb'] = theme('breadcrumb',  $breadcrumbs);
      }
    }

    //TODO Fix 'layout' variable and body classes (see template_preprocess_page()
    //     in theme.inc for reference

    $style = theme_get_setting('artistsC01_style');
    if ($style) { $arBodyClass[] = $style; }
    else { $arBodyClass[] = 'default'; }

    if (theme_get_setting('artistsC01_useSuckerfish')) {
      $vars['suckerfishPrimary'] = artistsC01_suckerfish_menu_tree(menu_tree_all_data('primary-links'));
    }

    // get color scheme
    $colorScheme = theme_get_setting('artistsC01_colorScheme');
    if (!isset($colorScheme)) { $colorScheme = 'blueLagoon'; }

    $arBodyClass[] = "colorScheme-" . $colorScheme;

    $customHeaderColor = theme_get_setting('artistsC01_customHeaderColor');
    if ($customHeaderColor) {
      $vars['customHeaderColor'] = $customHeaderColor;
    }

    $vars['logo'] = artistsC01_get_logo($colorScheme);

    $useCustomColumnWidths = theme_get_setting('artistsC01_useCustomColumnWidths');
    if ($useCustomColumnWidths) {
      $mainContentWidth = intval(theme_get_setting('artistsC01_mainContentWidth'));
      $sidebarWidth = intval(theme_get_setting('artistsC01_sidebarWidth'));
      if ($mainContentWidth < 75) { $mainContentWidth = 736; }
      if ($sidebarWidth < 25) { $sidebarWidth = 210; }

      $vars['artefUseCustomColumnWidths'] = true;

      $vars['artefWidth_span_full'] = $mainContentWidth + $sidebarWidth + 10;
      $vars['artefWidth_span_segments_full'] = $vars['artefWidth_span_full'] + 20;
      $vars['artefWidth_span_main_content'] = $mainContentWidth;
      $vars['artefWidth_span_main_content_padded'] = $mainContentWidth - 8;
      $vars['artefWidth_span_main_content_half'] = floor(($mainContentWidth-10)/2);
      $vars['artefWidth_span_main_content_half_padded'] = $vars['artefWidth_span_main_content_half'] - 8;
      $vars['artefWidth_span_main_content_third'] = floor(($mainContentWidth-10)/3);
      $vars['artefWidth_span_main_content_two_thirds'] = $mainContentWidth - $vars['artefWidth_span_main_content_third'] - 10;
      $vars['artefWidth_span_sidebar_normal'] = $sidebarWidth;
      $vars['artefWidth_span_sidebar_normal_padded'] = $sidebarWidth - 4;
    }

    $vars['artefBodyClass'] = implode($arBodyClass, ' ');

    return $vars;
}

function artistsC01_suckerfish_menu_item($menuItem, $maxDepthBelow, $class='') {
  $class .= ' menu-' . $menuItem['link']['mlid'];

  if (isset($menuItem['below'])) {
    if ($menuItem['link']['expanded']) { $class .= ' submenu expanded'; }
    else { $class .= ' submenu collapsed'; }
  }
  else { $class .= ' leaf'; }

  if (artef_isMenuItemInActiveTrail($menuItem)) {
    $class .= ' in-trail';
  }

//TODO  $class .= menu_in_active_trail($mid) ? ' active' : '';
  $class = trim($class);
  $output = '<li class="'. $class .'">';

  $link = $menuItem['link'];
  $link['options']['attributes']['class'] = trim($link['options']['attributes']['class'] . ' ' . $class);
  $link['localized_options']['attributes']['class'] = trim($link['localized_options']['attributes']['class'] . ' ' . $class);
  $innerHtml = l($link['title'], $link['href'], $link['localized_options']);

  if ($maxDepthBelow > 0 && is_array($menuItem['below'])) {
    $innerSuckerfishHtml = artistsC01_suckerfish_menu_tree($menuItem['below'], $maxDepthBelow-1);
    if (isset($innerSuckerfishHtml)) {
      // replace </a> with whats needed hack
      $innerHtml = substr($innerHtml, 0, -4);
      $innerHtml .=  '<!--[if IE 7]><!--></a><!--<![endif]--><!--[if lte IE 6]><table><tr><td><![endif]--><div class="suckerfish-secondary-wrapper">' . $innerSuckerfishHtml . '</div><!--[if lte IE 6]></td></tr></table></a><![endif]-->';
    }
  }

  $output .= $innerHtml . '</li>';

  return $output;
}

function artistsC01_suckerfish_menu_tree($suckerfishMenuTree, $maxDepthBelow = 1) {
  if (!isset($suckerfishMenuTree) || count($suckerfishMenuTree) == 0) {
    return;
  }

  if($maxDepthBelow < 0) {
    return;
  }

  $output .= '<ul class="suckerfish-primary">';

    // it has submenus
  $count = count($suckerfishMenuTree);
  $currentNo = 1;

//print_r($count);
//print_r($suckerfishMenuTree);

  foreach ($suckerfishMenuTree as $menuItem) {

    $class = '';
    //TODO if (menu_in_active_trail_in_submenu($cid, $parentMenuId)) {
    //  $class .= "-active active";
    //}
    if ($currentNo == 1) { $class .= " sf-first"; }
    if ($currentNo == $count) { $class .= " sf-last"; }

    $output .= artistsC01_suckerfish_menu_item($menuItem, $maxDepthBelow, $class);

    $currentNo++;
  }

  $output .= '</ul>';

  return $output;
}

/**
 * Generates breadcrumbs if turned on.
 *
 * @param array $breadcrumb
 * @return string
 */
function artistsC01_breadcrumb($breadcrumb) {

  if (!theme_get_setting('artistsC01_showBreadcrumbs')) {
    return;
  }

  if (!empty($breadcrumb)) {
    $output = '<div class="breadcrumb">';
    $isFirst = true;
    foreach ($breadcrumb as $curr) {
      if ($isFirst) {
        $output .= '<span class="breadcrumb-first">' . $curr . '</span>';
        $isFirst = false;
      }
      else {
        $output .= ' › ' . $curr;
      }
    }
    return $output . '</div>';
  }
}

/**
 * Overriden to insert theme help on theme configuration page.
 *
 * @return string
 */
function artistsC01_help() {
   return theme_help() . t(artistsC01_get_theme_help());
}

/**
 * Function returns html help for artistsC01 theme.
 *
 * @return string
 * help for artistsC01 configuration page
 */
function artistsC01_get_theme_help() {
  $output = '';

  if (arg(0)=='admin' && arg(1)=='build' && arg(2)=='block' && !arg(3))
  {
    $output .= '<h3><strong>artistsC01 block configuration</strong></h3>';
    $output .= '<p>Highlights regions and categories collapse and expand in special way. As other regions in this theme, if you leave region empty it will collapse. Collapsed right region does not modify layout. When left region colapses, right region in the same row will <strong>expand</strong> to take full <strong>width</strong>. </p>';
    $output .= '<p>Having in mind how highlights and categories collapse, one can configure four different layouts.</p>';
  }

  return $output;
}

/**
 * Return code that emits an XML icon.
 * Added theme code for theme population tracking.
 */
function artistsC01_xml_icon($url) {
  if ($image = theme('image', 'misc/xml.png', t('XML feed') . ' (C01 _th3me_)', t('XML feed'))) {
    return '<a href="'. check_url($url) .'" class="xml-icon">'. $image. '</a>';
  }
}

/**
 * Return code that emits an feed icon.
 * Added theme code for theme population tracking.
 */
function artistsC01_feed_icon($url) {
  if ($image = theme('image', 'misc/feed.png', t('Syndicate content') . ' (C01 _th3me_)', t('Syndicate content'))) {
    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image. '</a>';
  }
}


/**
* Declare the available regions implemented by this engine.
*
* @return
* An array of regions. Each array element takes the format:
* variable_name => t('human readable name')
*/
function artistsC01_regions() {
  return array(
    'header' => t('Header'),
    'banner_upper' => t('Banner Upper'),
    'highlight_left' => t('Highlight Left'),
    'highlight_right' => t('Highlight Right'),
    'banner_middle_1' => t('Banner Middle 1'),
    'banner_middle_2' => t('Banner Middle 2'),
    'content' => t('Content'),
    'category_1' => t('Category I Left'),
    'category_2' => t('Category I Right'),
    'category_3' => t('Category II Left'),
    'category_4' => t('Category II Right'),
    'category_5' => t('Category III Left'),
    'category_6' => t('Category III Right'),
    'category_7' => t('Category IV Left'),
    'category_8' => t('Category IV Right'),
    'banner_lower' => t('Banner Lower'),
    'right' => t('Right Sidebar'),
    'right_sidebar_box_1' => t('Right Sidebar Box 1'),
    'right_sidebar_box_2' => t('Right Sidebar Box 2'),
    'right_sidebar_box_3' => t('Right Sidebar Box 3'),
    'right_sidebar_box_4' => t('Right Sidebar Box 4'),
    'right_sidebar_box_5' => t('Right Sidebar Box 5'),
    'right_sidebar_box_6' => t('Right Sidebar Box 6'),
    'footer' => t('Footer')
    );
}

/**
 * Return path to logo image for given colorScheme
 */
function artistsC01_get_logo($colorScheme) {
  $logo = theme_get_setting('logo');

  if (theme_get_setting('toggle_logo')) {
    if (theme_get_setting('default_logo')) {
      $logo = base_path() . drupal_get_path('theme', 'artistsC01') . '/images/logo_' . $colorScheme . '.png';
    }
  }

  return $logo;
}

function artef_isMenuItemInActiveTrail($menuItem) {
  if (!isset($menuItem) || !isset($menuItem['link']['href'])) {
    return false;
  }

  $activeTrail = menu_get_active_trail();
  if (!isset($activeTrail)) {
    return false;
  }

  foreach ($activeTrail as $trailStep) {
    if ($trailStep['href'] == $menuItem['link']['href']) {
      return true;
    }
  }

  return false;
}

// DEVELOPMENT ONLY - REMOVE IN FINAL VERSION
drupal_rebuild_theme_registry();

// Following section adds css files based on theme settings

$style = theme_get_setting('artistsC01_style');

if ($style && $style != 'default') {
  drupal_add_css(drupal_get_path('theme', 'artistsC01') . '/css/custom_style_' . $style . '.css', 'theme');
}

$colorScheme = theme_get_setting('artistsC01_colorScheme');

if (!isset($colorScheme)) {
  $colorScheme = 'blueLagoon';
}

if ($colorScheme != 'plain') {
  drupal_add_css(drupal_get_path('theme', 'artistsC01') . '/css/custom_colorScheme_' . $colorScheme . '.css', 'theme');
}

drupal_add_css(drupal_get_path('theme', 'artistsC01') . '/css/custom_fonts.css', 'theme');
//TODO
//drupal_add_css(drupal_get_path('theme', 'artistsC01') . '/print.css', 'theme', 'print');

// Following section adds farbtastic css/js files and additional code for header background color settings
if ($_GET['q'] == 'admin/build/themes/settings/artistsC01' ) {
  drupal_add_css('misc/farbtastic/farbtastic.css', 'theme', 'all', FALSE);
  drupal_add_css(drupal_get_path('theme', 'artistsC01') . '/css/theme_settings.css', 'theme', 'all', FALSE);

  drupal_add_js('misc/farbtastic/farbtastic.js');

  $_logo_path = artistsC01_get_logo($colorScheme);
  $_logo_code = ($_logo_path) ? "<img src=\\\"$_logo_path\\\" />" : t('Preview');
  drupal_add_js('

  var colorbox;
  var preview;
  var farb;

  function _farb_init() {
    farb = $.farbtastic("#colorpicker");
    farb.linkTo(function (color) { callback(color) });

    colorbox = $("#edit-artistsC01-customHeaderColor");
    preview = $("#head_color_preview");

    preview.append("'. $_logo_code .'");

    colorbox.keyup(function() { farb.setColor(colorbox.val()) });

    // init
    callback(colorbox.val());
  }

  function callback(color) {

    colorbox.css({
      backgroundColor: color,
      color: farb.RGBToHSL(farb.unpack(color))[2] > 0.5 ? "#000" : "#fff"
    });
    preview.css({
      backgroundColor: color,
      color: farb.RGBToHSL(farb.unpack(color))[2] > 0.5 ? "#000" : "#fff"
    });

    colorbox.val(color);

  }

  $(document).ready(_farb_init);
',
  'inline');
}
