<?php

function artistsC01_settings($saved_settings) {

  $defaults = array(
    'artistsC01_style' => 0,
    'artistsC01_colorScheme' => 0,
    'artistsC01_customHeaderColor' => 0,
    'artistsC01_showBreadcrumbs' => 0,
    'artistsC01_includeIEPngFix' => 0,
  );

  $saved_settings = array_merge($defaults, $saved_settings);

  $form['artistsC01_style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#description' => t('By choosing different styles you set theme looks.'),
    '#default_value' => $saved_settings['artistsC01_style'],
    '#weight' => -7,
    '#options' => array(
      'default' => t('Default'),
      'smallBoxes' => t('Small Boxes'),
      'bigBorders' => t('Big Borders'),
      'plain' => t('Plain'),
    ),
  );

  $form['artistsC01_colorScheme'] = array(
    '#type' => 'select',
    '#title' => t('Color scheme'),
    '#description' => t('Choose color scheme from the list of pre-defined schemes.'),
    '#default_value' => $saved_settings['artistsC01_colorScheme'],
    '#weight' => -5,
    '#options' => array(
      'blueLagoon' => t('Blue Lagoon'),
      'nocturnal' => t('Nocturnal'),
      'shinyTomato' => t('Shiny Tomato'),
      'tealTop' => t('Teal Top'),
      'gray' => t('Gray'),
      'lime' => t('Lime'),
      'purple' => t('Purple'),
      'lightBrown' => t('Light Brown'),
      'magenta' => t('Magenta'),
      'cyan' => t('Cyan'),
      'turquoise' => t('Turquoise'),
      'plain' => t('Plain (no theme colors)')
    ),
  );

  $form['artistsC01_customHeaderSettings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom header settings'),
    '#description' => t('Custom header background-color settings (preview).'),
    '#weight' => -3,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['artistsC01_customHeaderSettings']['artistsC01_customHeaderColor'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom header color (in #rrggbb format, 0 to disable)'),
    '#description' => t('Choose custom color for header (useful for fitting the logo). As an alternative, you can also edit css/custom_colorScheme_X.css.'),
    '#default_value' => $saved_settings['artistsC01_customHeaderColor'],
    '#maxlength' => 9,
    '#size' => 9,
    '#prefix' => '<div id="head_color_preview"></div><div id="colorpicker"></div>'
    );

  $form['artistsC01_useSuckerfish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Suckerfish menu'),
    '#description' => t('Turn on or off suckerfish based menu. When turned on, it will generate menu using primary and secondary links.'),
    '#default_value' => $saved_settings['artistsC01_useSuckerfish'],
  );

  $form['artistsC01_showBreadcrumbs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show breadcrumbs'),
    '#description' => t('Turn the breadcrumbs on or off.'),
    '#default_value' => $saved_settings['artistsC01_showBreadcrumbs'],
  );

  $form['artistsC01_showCurrentInBreadcrumbs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show current page in breadcrumbs'),
    '#description' => t('Check this feature if you like to have current page in the breadcrumb.'),
    '#default_value' => $saved_settings['artistsC01_showCurrentInBreadcrumbs'],
  );

  $form['artistsC01_useCustomColumnWidths'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use custom column widths'),
    '#description' => t('Check this to have theme use customized column widths.'),
    '#default_value' => $saved_settings['artistsC01_useCustomColumnWidths'],
    );

  $form['artistsC01_mainContentWidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Main content column width (px, 0 for default)'),
    '#description' => t('Enter main content column width in pixels.'),
    '#default_value' => $saved_settings['artistsC01_mainContentWidth'],
    '#maxlength' => 4,
    '#size' => 4,
    );

  $form['artistsC01_sidebarWidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar column width (px, 0 for default)'),
    '#description' => t('Enter sidebar column width in pixels.'),
    '#default_value' => $saved_settings['artistsC01_sidebarWidth'],
    '#maxlength' => 4,
    '#size' => 4,
    );

  return $form;
}


