<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <!--[if lt IE 7]><style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() . '/conditional-le-ie6.css' ?>";</style><![endif]-->
  <?php if (isset($customHeaderColor) || $artefUseCustomColumnWidths) : ?>
    <style type="text/css" media="all">
      <?php if (isset($customHeaderColor)) : ?>
        #idSegmentUpper { background:<?php print $customHeaderColor; ?>; }
      <?php endif; ?>
      <?php if ($artefUseCustomColumnWidths) : ?>
        .container, .span-full { width:<?php print $artefWidth_span_full; ?>px; }
        .span-sidebar-normal { width:<?php print $artefWidth_span_sidebar_normal; ?>px; }
        .span-main-content { width:<?php print $artefWidth_span_main_content; ?>px; }
        .span-main-content-padded { width:<?php print $artefWidth_span_main_content_padded; ?>px; }
        .span-main-content-half { width:<?php print $artefWidth_span_main_content_half; ?>px; }
        .span-main-content-half-padded { width:<?php print $artefWidth_span_main_content_half_padded; ?>px; }
        .span-main-content-third { width:<?php print $artefWidth_span_main_content_third; ?>px; }
        .span-main-content-two-thirds { width:<?php print $artefWidth_span_main_content_two_thirds; ?>px; }
        .sidebar-block h2 { width:<?php print $artefWidth_span_sidebar_normal_padded; ?>px; }
        #idSegmentUpper, #idSegmentLower { min-width:<?php print $artefWidth_span_segments_full; ?>px; }
      <?php endif; ?>
    </style>
  <?php endif; ?>
  <?php print $scripts ?>
</head>
<body class="<?php print implode(array($body_classes, $artefBodyClass), ' '); ?>" >
  <div id="idOuter">
    <div id="idSegmentUpper">
      <div id="idHeader" class="container positioned-marker">
        <?php if (isset($header)) { print $header; } ?>
        <?php if (isset($logo) || $site_name || $site_slogan) : ?>
          <table class="layout site-header">
            <tbody><tr>
                <td><?php if (isset($logo)): ?> <?php print '<a href="' . check_url($base_path) . '" title="' . $site_name . '"><img src="'. check_url($logo) .'" alt="'. $site_name .'" id="logo" /></a>'; ?> <?php endif; ?></td>
                <td class="align-top"><table class="layout"><tbody>
                  <tr><td><?php if ($site_name): print '<h1 id="idSiteName" class="align-bottom offset-left"><a href="' . check_url($base_path) . '" title="' . $site_name . '">' . $site_name . '</a></h1>'; endif; ?></td></tr>
                  <tr><td><?php if ($site_slogan): print '<h1 id="idSiteSlogan" class="align-top offset-left">' . $site_slogan . '</h1>'; endif; ?></td></tr>
                </tbody></table></td>
              </tr>
            </tbody>
          </table>
        <?php endif; ?>
      </div>
      <?php if (!isset($suckerfishPrimary) && isset($primary_links)) : ?>
        <div id="idPrimaryLinks">
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        </div>
      <?php endif; ?>
    </div>
    <div id="idSegmentLower">
      <div class="container">
        <?php if (isset($suckerfishPrimary)) : ?>
          <div id="idSuckerfish">
            <?php print $suckerfishPrimary; ?>
          </div>
        <?php else : if (isset($secondary_links)) : ?>
          <div id="idSecondaryLinks">
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
          </div>
        <?php endif; ?>
        <?php endif; ?>
        <?php if ($breadcrumb): ?>
          <?php print $breadcrumb; ?>
          <?php if (!$banner_upper && !$highlight_left && !$highlight_right && !$banner_middle_1): ?>
            <div class="horizonal-spacer"><span>&nbsp;</span></div>
          <?php endif; ?>
        <?php else : ?>
          <div class="horizonal-spacer"><span>&nbsp;</span></div>
        <?php endif; ?>
        <div id="idCenter" class="column span-main-content first">
          <?php if ($banner_upper): ?>
          <div id="idRegionBannerUpper" class="column span-main-content region">
            <?php print $banner_upper; ?>
          </div>
          <?php endif; ?> <?php if ($highlight_left || $highlight_right): ?>
          <div class="column span-main-content region">
            <?php if ($highlight_left): ?>
            <div id="idRegionHighlightLeft" class="column first highlighted span-main-content-third">
              <?php print $highlight_left; ?>
            </div>
            <?php endif; ?> <?php if ($highlight_right): ?>
            <div id="idRegionHighlightRight" class="column highlighted<?php if ($highlight_left): print ' last span-main-content-two-thirds'; else: print ' first span-main-content'; endif; ?>">
              <?php print $highlight_right; ?>
            </div>
            <?php endif; ?>
          </div>
          <?php endif; ?> <?php if ($banner_middle_1): ?>
          <div id="idRegionBannerMiddle1" class="column span-main-content region">
            <?php print $banner_middle_1; ?>
          </div>
          <?php endif; ?>
          <div id="idRegionContent" class="column span-main-content main-content region">
            <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
            <?php if ($messages): print $messages; endif; ?>
            <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
              <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
            <?php if ($tabs): print $tabs .'</div>'; endif; ?>
            <?php if ($help): print $help; endif; ?>
            <?php print $content ?>
            <span class="clear"></span>
            <?php print $feed_icons ?>

            <pre><?php print_r(menu_set_active_trail()); ?></pre>
            <pre>
              <?php print_r(menu_tree_all_data("primary-links")); ?>
            </pre>

          </div>
          <?php if ($banner_middle_2): ?>
          <div id="idRegionBannerMiddle2" class="column span-main-content region">
            <?php print $banner_middle_2; ?>
          </div>
          <?php endif; ?>
          <div id="categories" class="region">
            <?php for ($artistsC01_Theme_i = 0; $artistsC01_Theme_i <= 3; $artistsC01_Theme_i++) : ?>
              <?php $artistsC01_Theme_CatA = 'category_' . (2*$artistsC01_Theme_i + 1); ?>
              <?php $artistsC01_Theme_CatB = 'category_' . (2*$artistsC01_Theme_i + 2); ?>
              <?php if (($$artistsC01_Theme_CatA) || ($$artistsC01_Theme_CatB)) : ?>
                <div class="column span-main-content category-pair">
                <?php if ($$artistsC01_Theme_CatA): ?>
                  <div id="idRegionCategory<?php print 2*$artistsC01_Theme_i + 1; ?>" class="column category first span-main-content-half-padded">
                    <?php print $$artistsC01_Theme_CatA; ?>
                  </div>
                <?php endif; ?>
                <?php if ($$artistsC01_Theme_CatB): ?>
                  <div id="idRegionCategory<?php print 2*$artistsC01_Theme_i + 2; ?>" class="column category<?php if ($$artistsC01_Theme_CatA): print ' last span-main-content-half-padded'; else: print ' first span-main-content-padded'; endif; ?>">
                    <?php print $$artistsC01_Theme_CatB; ?>
                  </div>
                <?php endif; ?>
                </div>
              <?php endif; ?>
            <?php endfor; ?>
          </div>
          <?php if ($banner_lower): ?>
          <div id="idRegionBannerLower" class="column span-main-content">
            <?php print $banner_lower; ?>
          </div>
          <?php endif; ?>
        </div>
        <div id="idRegionSidebar" class="column span-sidebar-normal last">
          <?php for ($artistsC01_Theme_i = 1; $artistsC01_Theme_i <= 6; $artistsC01_Theme_i++) : ?>
            <?php $artistsC01_Theme_right_sidebar_box = 'right_sidebar_box_' . $artistsC01_Theme_i; ?>
            <?php if ($$artistsC01_Theme_right_sidebar_box) : ?>
              <div id="idRegionSidebarBox<?php print $artistsC01_Theme_i; ?>" class="sidebar-block">
                <?php print $$artistsC01_Theme_right_sidebar_box; ?>
              </div>
            <?php endif; ?>
          <?php endfor; ?>
          <?php if ($right): ?> <?php print $right; ?> <?php endif; ?>
          <?php if ($left): ?> <?php print $left; ?> <?php endif; ?>
        </div>
        <div id="idRegionFooter" class="column small quiet footer span-100p">
          <?php print $footer; ?>
          <?php if ($footer_message) { print '<p>' . $footer_message . '</p>'; } ?>
          <p class="small"><a title="Artists C01 and other professional Drupal themes designed by ThemeArtists.com" href="http://themeartists.com/">Professional Drupal themes</a> by ThemeArtists.com</p>
        </div>
      </div>
    </div>
  </div>
<?php print $closure ?>
</body>
</html>
